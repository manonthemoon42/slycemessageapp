//
//  UIViewController+Alerts.swift
//  slyceMessage
//
//  Created by Sandro Tchikovani on 10/6/16.
//  Copyright © 2016 sandro. All rights reserved.
//

import UIKit

extension UIViewController {
    private class func alertWithButtonAction(title: String?,
                                             message: String?,
                                             actionTitle: String,
                                             actionHandler: ((UIAlertAction) -> Swift.Void)? = nil)
        -> UIAlertController {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: actionTitle, style: UIAlertActionStyle.default, handler: actionHandler))
            return alertController
    }
    
    /**
        Present an alert message
     
        @param title Title displayed in the Alert View
        @param message Message displayed in the Alert View
     */
    func presentAlertWithMessage(title: String, message: String) {
        let alertController = UIAlertController.alertWithButtonAction(title: title, message: message, actionTitle: "OK")
        self.present(alertController, animated: true, completion: nil);
    }
}
