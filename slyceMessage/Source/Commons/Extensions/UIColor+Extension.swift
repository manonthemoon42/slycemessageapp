//
//  UIColor+Extension.swift
//  slyceMessage
//
//  Created by Sandro Tchikovani on 2/10/17.
//  Copyright © 2017 Sandro Tchikovani. All rights reserved.
//

import UIKit

extension UIColor {
    open class var writerMessageColor: UIColor {
        return self.colorRGB(red: 50, green: 133, blue: 240)
    }
    
    open class var receivedMessageColor: UIColor {
        return self.colorRGB(red: 223, green: 223, blue: 223)
    }
    
    open class var lightGrayBackground: UIColor {
        return self.colorRGB(red: 246, green: 246, blue: 246)
    }
    
    /**
        Color RGB
        This allow to create a color without executing all the time the division by 255 to each RGB values.
    */
    class func colorRGB(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat = 1) -> UIColor {
        return UIColor(red:red/255 , green: green/255, blue: blue/255, alpha: alpha)
    }
}
