//
//  UIViewController+Media.swift
//  slyceMessage
//
//  Created by Sandro Tchikovani on 10/30/16.
//  Copyright © 2016 sandro. All rights reserved.
//

import AssetsLibrary
import MobileCoreServices
import Photos
import UIKit

extension UIViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    public func makeCameraAccessRequestAndGiveDirectAccess() {
        let authorizationStatus: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        if (authorizationStatus == .notDetermined) {
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (allow) in
                if (allow) {
                    self.presentImagePickerForSourceType(sourceType: .camera)
                } else {
                    self.presentAlertWithMessage(title: "Camera Access", message: "You denied access. We cannot display the camera")
                }
            })
        } else if authorizationStatus == .restricted {
            self.presentAlertWithMessage(title: "Camera Access", message: "Seems like you are not allowed to access to Camera")
        } else if authorizationStatus == .denied {
            self.presentAlertWithMessage(title: "Camera Access", message: "Go to settings for allowing access to the Camera")
        } else {
            self.presentImagePickerForSourceType(sourceType: .camera)
        }
    }
    
    func makePhotoLibraryAccessRequestAndGiveDirectAccess() {
        let authorizationStatus: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        if authorizationStatus == .notDetermined {
            PHPhotoLibrary.requestAuthorization() { status in
                if status == .authorized {
                    self.presentImagePickerForSourceType(sourceType: .photoLibrary)
                } else {
                    self.presentAlertWithMessage(title: "Photo Library", message: "You denied access. We cannot display the Photo Library")
                }
            }
        } else if authorizationStatus == .restricted {
            self.presentAlertWithMessage(title: "Photo Library", message: "Seems like you are not allowed to access to Photo Library")
        } else if authorizationStatus == .denied {
            self.presentAlertWithMessage(title: "Photo Library", message: "Go to settings for allowing access to the Photo Library")
        } else {
            self.presentImagePickerForSourceType(sourceType: .photoLibrary)
        }
    }
    
    /**
        Present Media Picker
     
        @param sourceType (Camera or Device Library)
     */
    fileprivate func presentImagePickerForSourceType(sourceType: UIImagePickerControllerSourceType) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = sourceType
        imagePicker.mediaTypes = [kUTTypeImage as String]
        
        self.present(imagePicker, animated: true, completion: nil)
    }
}
