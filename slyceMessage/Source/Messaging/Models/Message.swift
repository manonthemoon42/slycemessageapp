//
//  Message.swift
//  slyceMessage
//
//  Created by Sandro Tchikovani on 2/10/17.
//  Copyright © 2017 Sandro Tchikovani. All rights reserved.
//

import UIKit

class Message: NSObject {
    var message: String?
    var image: UIImage?
    var isPhoneUser: Bool = false
    
    init(text: String? = nil , imageAttached: UIImage? = nil) {
        image = imageAttached
        message = text
        super.init()
    }
}
