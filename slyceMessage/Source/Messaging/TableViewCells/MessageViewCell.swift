//
//  MessageTableViewCell.swift
//  slyceMessage
//
//  Created by Sandro Tchikovani on 2/14/17.
//  Copyright © 2017 Sandro Tchikovani. All rights reserved.
//

import UIKit

class MessageViewCell: UITableViewCell {
     var deleteMessageCallback: ((Message) -> Void)?
    
    @IBOutlet weak var messageTextView: UITextView! {
        didSet {
            messageTextView.layer.cornerRadius = 10
            
            let myMenuController: UIMenuController = UIMenuController.shared
            myMenuController.isMenuVisible = true
            myMenuController.arrowDirection = .down
            myMenuController.setTargetRect(.zero, in: messageTextView)
            
            let delete = UIMenuItem(title: "Delete", action: #selector(deleteMessage))
            myMenuController.menuItems = [delete]
        }
    }
    
    @IBOutlet var trailingLayoutConstraint: NSLayoutConstraint!
    @IBOutlet var leadingLayoutConstraint: NSLayoutConstraint!
    
    fileprivate var message: Message?
    
    // MARK: Cell Life Cycle
    override func prepareForReuse() {
        super.prepareForReuse()
        
        message = nil
        messageTextView.text = nil
    }
    
    func configure(message: Message) {
        self.message = message
        
        if message.isPhoneUser {
            messageTextView.backgroundColor = .writerMessageColor
            messageTextView.textColor = .white
        } else {
            messageTextView.backgroundColor = .receivedMessageColor
            messageTextView.textColor = .black
        }
        
        messageTextView.text = message.message
        setNeedsUpdateConstraints()
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(deleteMessage) {
            return true
        }
        return super.canPerformAction(action, withSender: sender)
    }
    
    func deleteMessage() {
        guard let deleteMessageCallback = deleteMessageCallback,
        let message = message else {
            return
        }
        
        deleteMessageCallback(message)
    }
}

// MARK: Auto Layout
extension MessageViewCell {
    override func updateConstraints() {
        guard let message = message else {
            super.updateConstraints()
            return
        }
        
        if message.isPhoneUser {
            leadingLayoutConstraint.isActive = false
            trailingLayoutConstraint.isActive = true
        } else {
            leadingLayoutConstraint.isActive = true
            trailingLayoutConstraint.isActive = false
        }
        
        super.updateConstraints()
    }
}
