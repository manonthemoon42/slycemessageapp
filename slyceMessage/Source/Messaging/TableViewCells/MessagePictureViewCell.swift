//
//  MessagePictureViewCell.swift
//  slyceMessage
//
//  Created by Sandro Tchikovani on 2/15/17.
//  Copyright © 2017 Sandro Tchikovani. All rights reserved.
//

import UIKit

class MessagePictureViewCell: UITableViewCell {
    /// IBOutlets
    @IBOutlet weak var attachedImageView: UIImageView! {
        didSet {
            attachedImageView.layer.cornerRadius = 10
            
            let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(showMenuController))
            attachedImageView.isUserInteractionEnabled = true
            attachedImageView.addGestureRecognizer(longPressRecognizer)
        }
    }

    fileprivate var message: Message?
    var deleteMessageCallback: ((Message) -> Void)?
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        attachedImageView.image = nil
        message = nil
    }
    
    func configure(message: Message) {
        self.message = message
        
        guard let image = message.image else {
            return
        }
        attachedImageView.image = image
    }
    
    /// MARK: Delete image
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(deleteMessage) {
            return true
        }
        return super.canPerformAction(action, withSender: sender)
    }
    
    @objc fileprivate func showMenuController() {
        becomeFirstResponder()
        let myMenuController = UIMenuController.shared
        myMenuController.arrowDirection = .down
        let delete = UIMenuItem(title: "Delete", action: #selector(deleteMessage))
        myMenuController.menuItems = [delete]
        myMenuController.setTargetRect(attachedImageView.frame, in: contentView)
        myMenuController.isMenuVisible = true
    }
    
    @objc fileprivate func deleteMessage() {
        guard let deleteMessageCallback = deleteMessageCallback,
            let message = message else {
                return
        }
        
        deleteMessageCallback(message)
    }
}
