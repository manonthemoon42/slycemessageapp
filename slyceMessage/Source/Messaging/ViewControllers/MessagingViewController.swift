//
//  MessagingViewController.swift
//  slyceMessage
//
//  Created by Sandro Tchikovani on 2/10/17.
//  Copyright © 2017 Sandro Tchikovani. All rights reserved.
//

import UIKit
import Photos

final class MessagingViewController: UIViewController {
    /// IBoutlets from nib
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.rowHeight = UITableViewAutomaticDimension
            tableView.estimatedRowHeight = 50
        }
    }
    
    @IBOutlet weak var screenViewContainer: UIView!
    
    @IBOutlet weak var textViewContainer: UIView! {
        didSet {
            textViewContainer.backgroundColor = .lightGrayBackground
        }
    }
    
    @IBOutlet weak var cameraButton: UIButton! {
        didSet {
            cameraButton.setImage(#imageLiteral(resourceName: "camera").withRenderingMode(.alwaysTemplate), for: .normal)
            cameraButton.tintColor = .black
        }
    }
    
    @IBOutlet weak var messageTextView: UITextView! {
        didSet {
            messageTextView.layer.borderColor = UIColor.lightGray.cgColor
            messageTextView.layer.borderWidth = 1
            messageTextView.layer.cornerRadius = 5
            messageTextView.delegate = self
            messageTextView.textColor = UIColor.lightGray
        }
    }
    
    @IBOutlet weak var sendButton: UIButton! {
        didSet {
            sendButton.isEnabled = false
        }
    }
    
    /// Auto Layout
    @IBOutlet weak var screenContainerBottomLayoutConstraint: NSLayoutConstraint!

    /// Data Messaging
    fileprivate var messages = [Message]()
    fileprivate var imageAttached: UIImage?
    
    /// UI Helper values
    fileprivate var kKeyboardHeight: CGFloat = 0
    fileprivate var kKeyboardAnimationSpeed: Double = 0
    fileprivate var cellHeights = [IndexPath: CGFloat]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Title Initialization
        self.title = "Slyce Contact"
        
        // TextView Placeholder initialization
        messageTextView.text = "iMessage"
        
        // Notifications Initialization
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: .UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillResignActive), name: .UIApplicationWillResignActive, object: nil)
        
        // Collection View Initialization
        let messageNib = UINib(nibName: String(describing: MessageViewCell.self), bundle:nil)
        let messagePictureNib = UINib(nibName: String(describing: MessagePictureViewCell.self), bundle:nil)
        tableView.register(messageNib, forCellReuseIdentifier: String(describing: MessageViewCell.self))
        tableView.register(messagePictureNib, forCellReuseIdentifier: String(describing: MessagePictureViewCell.self))

        // Messages Fetch Logic Initialization
        let messageManager = MessagingRequestManager.sharedInstance
        messageManager.newMessageReceivedCallback = { message in
            self.addNewMessage(message: message)
        }
        messageManager.startFetchingMessages()
    }
    
    deinit {
        MessagingRequestManager.sharedInstance.stopFetchingMessages()
        MessagingRequestManager.sharedInstance.newMessageReceivedCallback = nil
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK: Helper Methods
extension MessagingViewController {
    fileprivate func deleteMessage(message: Message) {
        guard let indexToRemove = messages.index(where: { $0 == message }) else {
            return
        }
        
        messages.remove(at: indexToRemove)
        
        tableView.beginUpdates()
        let indexPath = IndexPath(row: indexToRemove, section: 0)
        tableView.deleteRows(at: [indexPath], with: .fade)
        tableView.endUpdates()
    }
    
    fileprivate func resetMessageEnvironment() {
        messageTextView.text = "iMessage"
        messageTextView.textColor = UIColor.lightGray
        messageTextView.selectedTextRange = messageTextView.textRange(from: messageTextView.beginningOfDocument, to: messageTextView.beginningOfDocument)
        sendButton.isEnabled = false
        cameraButton.tintColor = .black
        imageAttached = nil
    }
    
    fileprivate func addNewMessage(message: Message) {
        messages.append(message)
        
        tableView.beginUpdates()
        let indexPath = IndexPath(row: messages.count - 1, section: 0)
        tableView.insertRows(at: [indexPath], with: .top)
        tableView.endUpdates()
        tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
    }
}

// MARK: UICollectionViewDatasource
extension MessagingViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard messages.indices.contains(indexPath.row) else {
            return UITableViewCell(style: .default, reuseIdentifier: String(describing: UITableViewCell.self))
        }
        
        let message = messages[indexPath.row]

        if message.image != nil {
            let messageCell = tableView.dequeueReusableCell(withIdentifier: String(describing: MessagePictureViewCell.self), for: indexPath) as? MessagePictureViewCell
            messageCell?.configure(message: message)
            messageCell?.deleteMessageCallback = deleteMessage
            return messageCell!
        } else {
            let messageCell = tableView.dequeueReusableCell(withIdentifier: String(describing: MessageViewCell.self), for: indexPath) as? MessageViewCell
            messageCell?.configure(message: message)
            messageCell?.deleteMessageCallback = deleteMessage
            return messageCell!
        }
    }
    
    /// Storing cell heights prevent jumpy effect when we insert new cell and scroll to the bottom
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let height = cell.frame.size.height
        cellHeights[indexPath] = height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = cellHeights[indexPath] {
            return height
        }
        return UITableViewAutomaticDimension
    }
}

// MARK: UITextViewDelegate
extension MessagingViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text as NSString
        let updatedText = currentText.replacingCharacters(in: range, with: text)
        
        if updatedText.isEmpty {
            resetMessageEnvironment()
            return false
        } else if textView.textColor == UIColor.lightGray && !text.isEmpty {
            textView.text = nil
            textView.textColor = UIColor.black
        }
        
        if !updatedText.isEmpty && textView.textColor != UIColor.lightGray {
            sendButton.isEnabled = true
        }
        
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
        UIView.animate(withDuration: kKeyboardAnimationSpeed, animations: {
            self.screenContainerBottomLayoutConstraint.constant = -self.kKeyboardHeight
        })
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        if self.view.window != nil {
            if textView.textColor == UIColor.lightGray {
                textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            }
        }
    }
}

// MARK: UIButton Actions
extension MessagingViewController {
    @IBAction func attachPhotoToMessage(_ sender: Any) {
        let actionSheet: UIAlertController = UIAlertController(title: "Attach Photo", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            actionSheet.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: { (action: UIAlertAction) in
                self.makeCameraAccessRequestAndGiveDirectAccess()
            }))
        }
        actionSheet.addAction(UIAlertAction(title: "Choose from Library", style: .default, handler: { (action: UIAlertAction) in
            self.makePhotoLibraryAccessRequestAndGiveDirectAccess()
        }))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func sendButtonTapped(_ sender: Any) {
        if let imageAttached = imageAttached {
            let newMessage = Message(imageAttached: imageAttached)
            newMessage.isPhoneUser = true
            addNewMessage(message: newMessage)
        }
        if messageTextView.textColor != UIColor.lightGray, let messageText = messageTextView.text, messageText.characters.count > 0 {
            let newMessage = Message(text: messageText)
            newMessage.isPhoneUser = true
            addNewMessage(message: newMessage)
        }
        resetMessageEnvironment()
    }
}

// MARK: UIIMagePickerDelegate
extension MessagingViewController {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        let mediaAssetURL = info["UIImagePickerControllerReferenceURL"] as! URL?
        let mediaOriginalImage = info["UIImagePickerControllerOriginalImage"] as! UIImage?
        
        if let assetURL = mediaAssetURL {
            /// Image has been taken from camera
            guard let mediaAsset = PHAsset.fetchAssets(withALAssetURLs: [assetURL], options: nil).firstObject else {
                /// Issue fetching the image...
                return
            }
            
            let phManager: PHImageManager = PHImageManager.default()
            let targetSize = CGSize(width: 170, height: 170)
            
            phManager.requestImage(for: mediaAsset, targetSize: targetSize, contentMode: .aspectFill, options: nil) {
                result, info in
                self.imageAttached = result
            }
        } else if let mediaOriginalImage = mediaOriginalImage{
            /// Image picked in library, so it's ready
            imageAttached = mediaOriginalImage
        }
        
        cameraButton.tintColor = UIColor.colorRGB(red: 0, green: 122, blue: 255)
        sendButton.isEnabled = true
    }
}

// MARK: Notifications
extension MessagingViewController {
    @objc fileprivate func keyboardWillShow(notification: Notification) {
        if let keyboardSize = notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? CGRect {
            kKeyboardHeight = keyboardSize.height
        }
        
        if let animationSpeed = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? Double {
            kKeyboardAnimationSpeed = animationSpeed
        }
    }
    
    @objc fileprivate func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: kKeyboardAnimationSpeed, animations: {
            self.screenContainerBottomLayoutConstraint.constant = 0
        })
    }
    
    @objc fileprivate func applicationDidBecomeActive() {
        MessagingRequestManager.sharedInstance.startFetchingMessages()
    }
    
    @objc fileprivate func applicationWillResignActive() {
        MessagingRequestManager.sharedInstance.stopFetchingMessages()
    }
}

// MARK: Swipe Gesture Recognizer
extension MessagingViewController: UITableViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if screenContainerBottomLayoutConstraint.constant > 0 {
            UIView.animate(withDuration: kKeyboardAnimationSpeed, animations: {
                self.screenContainerBottomLayoutConstraint.constant = 0
            })
        }
    }
}



