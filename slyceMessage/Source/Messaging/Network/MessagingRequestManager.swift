//
//  MessagingRequestManager.swift
//  slyceMessage
//
//  Created by Sandro Tchikovani on 2/11/17.
//  Copyright © 2017 Sandro Tchikovani. All rights reserved.
//

import Foundation

class MessagingRequestManager {
    static let sharedInstance = MessagingRequestManager()
    
    fileprivate let messages = [
        "Im bored",
        "I wish I could go to spain right now 🇪🇸",
        "To be on the beach, sun, cocktail. 😎",
        "Do you think I reapeat myself too much?",
        "💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽💪🏽",
        "It's really time for me to work out 💪🏼. I'm out of shape",
        "Would you rather be Superman or Spiderman?"
    ]
    
    private var messageFetchTimer: Timer?
    var newMessageReceivedCallback: ((Message) -> Void)?
    
    /**
        Start Fetching Messages
     
        This will execute the timer and will create a message every 5 seconds.
        It will pick a message from the array messages.
        
        If caller need to receive the messages, need to make sure to set the callback newMessageReceivedCallback.
    */
    func startFetchingMessages() {
        guard messageFetchTimer == nil else {
            return
        }
        
        messageFetchTimer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true, block: { _ in
            guard let receiverCallback = self.newMessageReceivedCallback else {
                return
            }
            
            let randomIndex = arc4random_uniform(UInt32(self.messages.count))
            let messageText = self.messages[Int(randomIndex)]
            
            let message = Message(text: messageText)
            receiverCallback(message)
        })
    }
    
    /**
        Stop Fetching Message
        This will stop the timer, and will stop creating messages every 5 seconds.
    */
    func stopFetchingMessages() {
        messageFetchTimer?.invalidate()
        messageFetchTimer = nil
    }
}
